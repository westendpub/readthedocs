Help and Documentation
======================

**West End House Pub** is happy to offer email and cloud storage solutions.
It uses many `iRedMail <https://www.iredmail.org/>`_ and `Nextcloud <https://nextcloud.com/>`_
and offers many *simple* and *intuitive* GUIs for webmail.

Check out the :doc:`email` section for further information, including
how to setup :ref:`clients`.

.. note::

   This project is under active development.

Contents
--------

.. toctree::

   email
   cloud
